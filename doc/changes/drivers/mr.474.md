psvr: We were sending in the wrong type of time to the 3DOF fusion code,
switch to nanoseconds instead of fractions of seconds.

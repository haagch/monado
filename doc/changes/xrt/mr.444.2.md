Make `xrt_compositor::create_swapchain` return xrt_result_t instead of the
swapchain, this makes the methods on `xrt_compositor` more uniform.

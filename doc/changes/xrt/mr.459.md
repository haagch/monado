Add `XRT_SWAPCHAIN_USAGE_INPUT_ATTACHMENT` flag to `xrt_swapchain_usage_bits`
so that a client can create a Vulkan swapchain that can be used as input
attachment.

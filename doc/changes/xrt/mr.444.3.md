Add the method `xrt_compositor::import_swapchain` allowing a state tracker to
create a swapchain from a set of pre-allocated images. Uses the same
`xrt_swapchain_create_info` as `xrt_compositor::create_swapchain`.

---
- mr.428
---
Rename functions and types that assumed the native graphics buffer handle type was an FD: in `auxiliary/vk/vk_helpers.{h,c}` `vk_create_image_from_fd` -> `vk_create_image_from_native`, in the XRT headers `struct xrt_compositor_fd` -> `xrt_compositor_native` (and method name changes from `xrt_comp_fd_...` -> `xrt_comp_native_...`), `struct xrt_swapchain_fd` -> `struct xrt_swapchain_native`, `struct xrt_image_fd` -> `struct xrt_image_native`, and corresponding parameter/member/variable name changes (e.g. `struct xrt_swapchain_fd *xscfd` becomes `struct xrt_swapchain_native *xscn`).

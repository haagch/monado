Make some fields on `xrt_gl_swapchain` and `xrt_vk_swapchain` private moving
them into the client compositor code instead of exposing them.
